#!/bin/bash

[ -d output ] || mkdir output

timestamp=$(date +%d-%m-%Y_%H-%M-%S)
script_file=$1
outfile="./output/log_$timestamp.json"

yarn artillery run --target "https://157.230.226.212" --insecure -o $outfile $script_file

yarn artillery report $outfile -o ./output/index.html