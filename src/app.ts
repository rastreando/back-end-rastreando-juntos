import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import mongoose from 'mongoose';
import morgan from 'morgan';
import path from 'path';
import { DATABASE_URL, HOST, HTTP_PORT } from './.resources/config';
import errorMiddleware from './middlewares/error';
import routes from './routes';

// const key = readFileSync(path.join(__dirname, '..', 'rsa_keys', 'keyTest.pem')); // mudar para chaves quando mandar para producao
// const cert = readFileSync(
//   path.join(__dirname, '..', 'rsa_keys', 'certTest.pem')
// ); // mudar para chaves quando mandar para producao

export const createServer = async () => {
  mongoose
    .connect(DATABASE_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      ignoreUndefined: true,
    })
    .then((m) => {
      console.log(`conectado ao banco: ${m.connection.name}`);
    });

  const app = express();

  app.use(express.json());
  app.use(helmet());
  app.use(cors());
  app.use(morgan('dev'));

  app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')));

  app.use(routes);

  app.use(errorMiddleware);

  // const server = https.createServer({ key, cert }, app);

  // server.listen(PORT, HOST, () =>
  //   console.log(`servidor rodando em: https://${HOST}:${PORT}`)
  // );

  app.listen(HTTP_PORT, HOST, () =>
    console.log(`servidor rodando em: http://${HOST}:${HTTP_PORT}`)
  );
};
