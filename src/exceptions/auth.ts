import HttpException from './http_exception';
import { StatusCodes } from 'http-status-codes';

export class WrongPasswordException extends HttpException {
  constructor() {
    super(StatusCodes.UNAUTHORIZED, 'Senha invalida');
  }
}

export class TokenNotFound extends HttpException {
  constructor() {
    super(StatusCodes.BAD_REQUEST, 'token nao informado');
  }
}

export class InvalidToken extends HttpException {
  constructor() {
    super(StatusCodes.UNAUTHORIZED, 'token invalido');
  }
}

export class MalformedToken extends HttpException {
  constructor() {
    super(StatusCodes.BAD_REQUEST, 'token possui formato incorreto');
  }
}

export class CannotGenerateToken extends HttpException {
  constructor() {
    super(StatusCodes.INTERNAL_SERVER_ERROR, 'nao foi possivel gerar o token');
  }
}
