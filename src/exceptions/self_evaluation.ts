import { StatusCodes } from 'http-status-codes';
import HttpException from './http_exception';

export class formsNotFoundException extends HttpException {
  constructor() {
    super(StatusCodes.NOT_FOUND, `Formularios não encontrados`);
  }
}

export class FormAlreadyExistsException extends HttpException {
  constructor() {
    super(StatusCodes.BAD_REQUEST, `formulário já existe`);
  }
}

export class CannotUpdateFormException extends HttpException {
  constructor() {
    super(
      StatusCodes.INTERNAL_SERVER_ERROR,
      `Formulário nâo pôde ser atualizado`
    );
  }
}

export class CannotDeleteFormException extends HttpException {
  constructor() {
    super(
      StatusCodes.INTERNAL_SERVER_ERROR,
      `Formulário nâo pôde ser deletado`
    );
  }
}
