import { StatusCodes } from 'http-status-codes';
import HttpException from './http_exception';

export class CannotDeleteDiagnosticException extends HttpException {
  constructor() {
    super(
      StatusCodes.INTERNAL_SERVER_ERROR,
      `Diagnóstico nâo pôde ser deletado`
    );
  }
}
