import { StatusCodes } from 'http-status-codes';
import HttpException from './http_exception';

export class UserNotFoundException extends HttpException {
  constructor() {
    super(StatusCodes.NOT_FOUND, `Usuario nao encontrado`);
  }
}

export class UserAlreadyExistsException extends HttpException {
  constructor() {
    super(StatusCodes.BAD_REQUEST, `Usuario já existe`);
  }
}

export class CannotUpdateUserException extends HttpException {
  constructor() {
    super(StatusCodes.INTERNAL_SERVER_ERROR, `Usuario nao pôde ser atualizado`);
  }
}

export class CannotDeleteUserException extends HttpException {
  constructor() {
    super(StatusCodes.INTERNAL_SERVER_ERROR, `Usuario nao pôde ser deletado`);
  }
}
