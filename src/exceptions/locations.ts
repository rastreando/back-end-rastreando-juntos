import { StatusCodes } from 'http-status-codes';
import HttpException from './http_exception';

export class CannotDeleteLocationException extends HttpException {
  constructor() {
    super(
      StatusCodes.INTERNAL_SERVER_ERROR,
      `localização nâo pôde ser deletada`
    );
  }
}
