import ExtendableError from './base_interface';

export default class HttpException extends ExtendableError {
  status: number;

  constructor(httpStatusCode: number, message: string) {
    super(message);
    this.status = httpStatusCode;
  }
}
