export function rawDateToUtc(rawDate: string): Date {
  return new Date(new Date(rawDate).toUTCString());
}
