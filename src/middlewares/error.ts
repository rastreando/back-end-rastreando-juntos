import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import HttpException from '../exceptions/http_exception';

export default function errorMiddleware(
  error: HttpException,
  _: Request,
  res: Response,
  __: NextFunction
): Response {
  const status = error.status || StatusCodes.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Um erro ocorreu no servidor';
  const name = error.name || undefined;

  console.log(error);
  return res.status(status).json({ name, status, message });
}
