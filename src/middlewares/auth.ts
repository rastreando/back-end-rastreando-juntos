import { NextFunction, Response, Request } from 'express';
import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../.resources/config';
import {
  InvalidToken,
  MalformedToken,
  TokenNotFound,
} from '../exceptions/auth';
import { JwtPayload } from '../.resources/types';

export default function authMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const { authorization } = req.headers;

  if (!authorization) {
    return next(new TokenNotFound());
  }
  const [scheme, token] = authorization.split(' ');
  if (!scheme || !/^Bearer$/i.test(scheme) || !token) {
    return next(new MalformedToken());
  }

  jwt.verify(token, JWT_SECRET, (err: any, payload) => {
    if (err) {
      return next(new InvalidToken());
    }
    res.locals.session = {
      userId: (<JwtPayload>payload).id,
    };
    return next();
  });
}
