import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import HttpException from '../exceptions/http_exception';

export class Unauthorized extends HttpException {
  constructor() {
    super(StatusCodes.UNAUTHORIZED, 'nao é possivel acessar esta rota');
  }
}

export default function unauthorizedMiddleware(
  _: Request,
  __: Response,
  next: NextFunction
): void {
  return next(new Unauthorized());
}
