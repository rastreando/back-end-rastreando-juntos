import HttpException from '../exceptions/http_exception';
import { CannotDeleteLocationException } from '../exceptions/locations';
import schema, { ILocations } from '../models/locations';
import { rawDateToUtc } from '../services/general_date_service';
// import GeoJson from 'mongoose-geojson-schema';

// Construir testes
class LocationsRepository {
  findAll() {
    return schema.find().lean().orFail().exec();
  }

  findAllLocationsByUser(userId: string) {
    // Restringir por data? (sim, os ultimos 15 dias podem ser interessantes)
    return schema.find({ userId }).lean().exec();
  }

  async saveLocation(data: ILocations) {
    try {
      return schema.create({
        ...data,
        data_coleta: rawDateToUtc(data.data_coleta.toString()),
      });
    } catch (e) {
      throw new HttpException(500, 'internal server error');
    }
  }

  //   updateForm(formId: string, data: Partial<ISelfEvaluation>) {
  //     return schema
  //     .findOneAndUpdate({ _id: formId }, data, {
  //       new: true,
  //       useFindAndModify: false,
  //     })
  //     .lean()
  //     .orFail(new CannotUpdateFormException())
  //     .exec();
  //   } // deveria alterar o campo updatedAt apenas e nao o createdAt

  deleteLocation(locationId: string) {
    return schema
      .findOneAndRemove({ _id: locationId })
      .lean()
      .orFail(new CannotDeleteLocationException())
      .exec();
  }
}

export default new LocationsRepository();
