import bcrypt from 'bcrypt';
import {
  CannotUpdateUserException,
  UserAlreadyExistsException,
  UserNotFoundException,
} from '../exceptions/user';
import schema, { IUser } from '../models/users';

// Faltou documentar o objetivo de cada metodo ou refazer os nomes, por exemplo "save" nao eh um bom nome para um metodo,
// o que esta sendo salvo?
class UserRepository {
  findAll() {
    return schema.find().lean().orFail().exec();
  }

  findByUsername(username: string) {
    return schema
      .findOne({
        username: {
          $eq: username,
        },
      })
      .orFail(new UserNotFoundException())
      .lean()
      .exec();
  }

  findById(id: string) {
    return schema
      .findById(id)
      .lean()
      .orFail(new UserNotFoundException())
      .exec();
  }

  async save(data: IUser) {
    try {
      await this.findByUsername(data.email);
      throw new UserAlreadyExistsException();
    } catch (e) {
      return schema.create(data);
    }
  }

  async updateOne(id: string, data: Partial<IUser>) {
    return schema
      .findOneAndUpdate(
        { _id: id },
        {
          ...data,
          senha: data.senha ? await bcrypt.hash(data.senha, 10) : undefined,
        },
        {
          new: true,
          useFindAndModify: false,
          omitUndefined: true,
        }
      )
      .lean()
      .orFail(new CannotUpdateUserException())
      .exec();
  }
}

export default new UserRepository();
