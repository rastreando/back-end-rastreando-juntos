import HttpException from '../exceptions/http_exception';
import {
  CannotDeleteFormException,
  CannotUpdateFormException,
} from '../exceptions/self_evaluation';
import schema, { ISelfEvaluation } from '../models/self_evaluation';
import { rawDateToUtc } from '../services/general_date_service';

// Construir testes
class SelfEvaluationRepository {
  findAll() {
    return schema.find().lean().orFail().exec();
  }

  findAllFormsByUser(userId: string) {
    // Restringir por data? (sim, os ultimos 15 dias podem ser interessantes)
    return schema.find({ userId }).lean().exec();
  }

  async saveForm(data: ISelfEvaluation) {
    try {
      return schema.create({
        ...data,
        data_preenchimento: rawDateToUtc(data.data_preenchimento.toString()),
      });
    } catch (e) {
      throw new HttpException(500, 'internal server error');
    }
  }

  updateForm(formId: string, data: Partial<ISelfEvaluation>) {
    return schema
      .findOneAndUpdate({ _id: formId }, data, {
        new: true,
        useFindAndModify: false,
      })
      .lean()
      .orFail(new CannotUpdateFormException())
      .exec();
  } // deveria alterar o campo updatedAt apenas e nao o createdAt

  deleteForm(formId: string) {
    return schema
      .findOneAndRemove({ _id: formId })
      .lean()
      .orFail(new CannotDeleteFormException())
      .exec();
  }
}

export default new SelfEvaluationRepository();
