import { CannotDeleteDiagnosticException } from '../exceptions/diagnostic';
import HttpException from '../exceptions/http_exception';
import schema, { IDiagnostic } from '../models/diagnostic';
import { rawDateToUtc } from '../services/general_date_service';

// Construir testes
class DiagnosticRepository {
  findAll() {
    return schema.find().lean().orFail().exec();
  }

  findAllDiagnosticsByUser(userId: string) {
    // Restringir por data? (sim, os ultimos 15 dias podem ser interessantes)
    return schema.find({ userId }).lean().exec();
  }

  findDiagnosticByDate(userId: string, utcDate: Date) {
    return schema
      .find({
        userId: userId,
        data_resultado: {
          $gte: new Date(utcDate.setHours(0, 0, 0)),
          $lt: new Date(utcDate.setHours(23, 59, 59)),
        },
      })
      .lean()
      .exec();
  }

  async saveDiagnostic(data: IDiagnostic) {
    try {
      return schema.create({
        ...data,
        data_resultado: rawDateToUtc(data.data_resultado.toString()),
      });
    } catch (e) {
      throw new HttpException(500, 'internal server error');
    }
  }

  //   updateForm(formId: string, data: Partial<ISelfEvaluation>) {
  //     return schema
  //     .findOneAndUpdate({ _id: formId }, data, {
  //       new: true,
  //       useFindAndModify: false,
  //     })
  //     .lean()
  //     .orFail(new CannotUpdateFormException())
  //     .exec();
  //   } // deveria alterar o campo updatedAt apenas e nao o createdAt

  deleteDiagnostic(diagnosticId: string) {
    return schema
      .findOneAndRemove({ _id: diagnosticId })
      .lean()
      .orFail(new CannotDeleteDiagnosticException())
      .exec();
  }
}

export default new DiagnosticRepository();
