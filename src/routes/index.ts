import { Router as createRouter } from 'express';
import v1 from './api_v1';

const router = createRouter();

router.use('/health', (_, res) => {
  res.status(200).json({
    message: 'API Online',
    timestamp: new Date().toISOString(),
  });
});

router.use('/api/v1', v1); // v1 eh a versao 1 do aplicativo?

export default router;
