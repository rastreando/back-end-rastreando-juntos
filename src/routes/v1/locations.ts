import { Router as createRouter } from 'express';
import controller from '../../controllers/locations';
import authMiddleware from '../../middlewares/auth';
import unauthorizedMiddleware from '../../middlewares/unauthorized';

const router = createRouter();

router.use(authMiddleware);

router.get('/by-user', controller.showLocations);
router
  .route('/')
  .post(controller.saveLocation)
  .get(unauthorizedMiddleware, controller.index);
router
  .route('/:id') // id das localizacoes por usuario
  .delete(controller.deleteLocation);

export default router;
