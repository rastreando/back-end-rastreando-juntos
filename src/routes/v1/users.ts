import { Router as createRouter } from 'express';
import controller from '../../controllers/users'; // Nao abreviar variaveis, dificulta o entendimento do codigo
import unauthorizedMiddleware from '../../middlewares/unauthorized';

const router = createRouter();

router
  .route('/')
  .post(controller.save)
  .get(unauthorizedMiddleware, controller.index);
router
  .route('/:id')
  .get(controller.show)
  .delete(controller.delete)
  .put(controller.update);

export default router;
