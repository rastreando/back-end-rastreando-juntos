import { Router as createRouter } from 'express';
import controller from '../../controllers/self_evaluation';
import authMiddleware from '../../middlewares/auth';
import unauthorizedMiddleware from '../../middlewares/unauthorized';

const router = createRouter();

router.use(authMiddleware);

router.get('/by-user', controller.showForm);
router
  .route('/')
  .post(controller.saveForm)
  .get(unauthorizedMiddleware, controller.index);
router
  .route('/:id') // id do formulario para sua atualizacao
  .put(controller.updateForm)
  .delete(controller.deleteForm);

export default router;
