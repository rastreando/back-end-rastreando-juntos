import { Router as createRouter } from 'express';
import controller from '../../controllers/diagnostic';
import authMiddleware from '../../middlewares/auth';
import unauthorizedMiddleware from '../../middlewares/unauthorized';

const router = createRouter();

router.use(authMiddleware);

router.get('/by-user', controller.showDiagnostic);
router.get('/by-user/by-date', controller.showDiagnosticByDate);
router
  .route('/')
  .post(controller.saveDiagnostic)
  .get(unauthorizedMiddleware, controller.index);
router
  .route('/:id') // id do diagnostico
  .delete(controller.deleteDiagnostic);

export default router;
