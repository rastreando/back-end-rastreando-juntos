import { Router as createRouter } from 'express';
import authController from '../controllers/auth';
import authMiddleware from '../middlewares/auth';
import userRoutes from './v1/users';
import selfEvaluationRoutes from './v1/self_evaluation';
import diagnosticRoutes from './v1/diagnostic';
import locationsRoutes from './v1/locations';

const router = createRouter();

router.post('/login', authController.login);
router.post('/register', authController.register);

router.get('/auth/refresh-token', authController.refreshToken);
router.post('/auth/verify-token', authMiddleware, (_, res) => {
  return res.json({ ok: true });
});

router.use('/users', userRoutes);
router.use('/self-evaluation', selfEvaluationRoutes);
router.use('/diagnostics', diagnosticRoutes);
router.use('/locations', locationsRoutes);

export default router;
