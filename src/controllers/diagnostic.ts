import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import { IDiagnostic } from '../models/diagnostic';
import diagnosticRepository from '../repositories/diagnostic';
import { rawDateToUtc } from '../services/general_date_service';

class DiagnosticController {
  index: RequestHandler = async (_, res, next) => {
    try {
      return res
        .status(StatusCodes.OK)
        .json(await diagnosticRepository.findAll());
    } catch (err) {
      next(err);
    }
  };

  showDiagnostic: RequestHandler = async (_, res, next) => {
    const { userId } = res.locals.session;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await diagnosticRepository.findAllDiagnosticsByUser(userId));
    } catch (err) {
      return next(err);
    }
  };

  showDiagnosticByDate: RequestHandler = async (req, res, next) => {
    const { userId } = res.locals.session;
    const { date } = req.query;

    try {
      return res
        .status(StatusCodes.OK)
        .json(
          await diagnosticRepository.findDiagnosticByDate(
            userId,
            rawDateToUtc(<string>date)
          )
        );
    } catch (err) {
      return next(err);
    }
  };

  saveDiagnostic: RequestHandler = async (req, res, next) => {
    const { body } = req;
    const { userId } = res.locals.session;

    try {
      return res.status(StatusCodes.CREATED).json(
        await diagnosticRepository.saveDiagnostic(<IDiagnostic>{
          userId,
          ...body,
        })
      );
    } catch (err) {
      next(err);
    }
  };

  deleteDiagnostic: RequestHandler = async (req, res, next) => {
    const { id } = req.params;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await diagnosticRepository.deleteDiagnostic(id));
    } catch (err) {
      next(err);
    }
  };
}

export default new DiagnosticController();
