import bcrypt from 'bcrypt';
import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../.resources/config';
import { JwtPayload } from '../.resources/types';
import {
  CannotGenerateToken,
  InvalidToken,
  MalformedToken,
  TokenNotFound,
  WrongPasswordException,
} from '../exceptions/auth';
import {
  UserAlreadyExistsException,
  UserNotFoundException,
} from '../exceptions/user';
import { IUser } from '../models/users';
import UserRepo from '../repositories/user';

class AuthController {
  refreshToken: RequestHandler = async (req, res, next) => {
    const { id } = req.body;

    try {
      const user = await UserRepo.findById(id);

      const token = jwt.sign(<JwtPayload>{ id: user._id }, JWT_SECRET, {
        expiresIn: '30d', // talvez o tempo de expiracao de um token possa ser maior, como o uso eh no smartphone
      });

      return res.status(StatusCodes.OK).json({
        token,
      });
    } catch (err) {
      if (err instanceof UserNotFoundException) {
        next(err);
      } else {
        next(new CannotGenerateToken());
      }
    }
  };

  verifyToken: RequestHandler = async (req, res, next) => {
    const { authorization } = req.headers;

    if (!authorization) {
      return next(new TokenNotFound());
    }

    const [scheme, token] = authorization.split(' ');
    if (!scheme || !/^Bearer$/i.test(scheme) || !token) {
      return next(new MalformedToken());
    }

    return jwt.verify(token, JWT_SECRET, (err: any, _) => {
      if (err) {
        return next(new InvalidToken());
      }
      return res.status(StatusCodes.OK).json({
        ok: true,
      });
    });
  };

  login: RequestHandler = async (req, res, next) => {
    const { username, senha } = req.body;

    try {
      const user = await UserRepo.findByUsername(username);

      if (!(await bcrypt.compare(senha, user.senha))) {
        throw new WrongPasswordException();
      } else {
        return res.status(StatusCodes.OK).json({
          ...user,
          token: jwt.sign(<JwtPayload>{ id: user._id }, JWT_SECRET, {
            expiresIn: '30d',
          }),
        });
      }
    } catch (err) {
      next(err);
    }
  };

  register: RequestHandler = async (req, res, next) => {
    const { body } = req;

    try {
      const { username } = body as IUser;
      await UserRepo.findByUsername(username);
      throw new UserAlreadyExistsException();
    } catch (err) {
      if (!(err instanceof UserNotFoundException)) {
        return next(err);
      }

      try {
        const user = await UserRepo.save({
          ...body,
          senha: await bcrypt.hash(body.senha, 10),
        });

        return res.status(StatusCodes.OK).json(user);
      } catch (err) {
        next(err);
      }
    }
  };
}

export default new AuthController();
