import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import { IDiagnostic } from '../models/diagnostic';
import { ISelfEvaluation } from '../models/self_evaluation';
import diagnosticRepository from '../repositories/diagnostic';
import selfEvaluationRepository from '../repositories/self_evaluation';

class SelfEvaluationController {
  index: RequestHandler = async (_, res, next) => {
    try {
      return res
        .status(StatusCodes.OK)
        .json(await selfEvaluationRepository.findAll());
    } catch (err) {
      next(err);
    }
  };

  showForm: RequestHandler = async (_, res, next) => {
    const { userId } = res.locals.session;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await selfEvaluationRepository.findAllFormsByUser(userId));
    } catch (err) {
      return next(err);
    }
  };

  saveForm: RequestHandler = async (req, res, next) => {
    const { auto_avaliacao: autoAvaliacaoData, diagnostico: diagnosticoData } =
      req.body;
    const { userId } = res.locals.session;

    try {
      return res.status(StatusCodes.CREATED).json({
        'auto-avaliacao': await selfEvaluationRepository.saveForm(<
          ISelfEvaluation
        >{
          ...autoAvaliacaoData,
          userId,
        }),
        diagnostico: await diagnosticRepository.saveDiagnostic(<IDiagnostic>{
          ...diagnosticoData,
          userId,
        }),
      });
    } catch (err) {
      next(err);
    }
  };

  updateForm: RequestHandler = async (req, res, next) => {
    const { id } = req.params;
    const { body } = req;
    const { userId } = res.locals.session;

    try {
      return res.status(StatusCodes.OK).json(
        await selfEvaluationRepository.updateForm(id, <ISelfEvaluation>{
          userId,
          ...body,
        })
      );
    } catch (err) {
      next(err);
    }
  };

  deleteForm: RequestHandler = async (req, res, next) => {
    const { id } = req.params;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await selfEvaluationRepository.deleteForm(id));
    } catch (err) {
      next(err);
    }
  };
}

export default new SelfEvaluationController();
