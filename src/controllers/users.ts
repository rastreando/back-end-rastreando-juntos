import bcrypt from 'bcrypt';
import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import { IUser } from '../models/users';
import userRepository from '../repositories/user';

class UserController {
  index: RequestHandler = async (_, res, next) => {
    try {
      return res.status(StatusCodes.OK).json(await userRepository.findAll());
    } catch (err) {
      next(err);
    }
  };

  show: RequestHandler = async (req, res, next) => {
    const { id } = req.params;

    try {
      return res.status(StatusCodes.OK).json(await userRepository.findById(id));
    } catch (err) {
      next(err);
    }
  };

  save: RequestHandler = async (req, res, next) => {
    const { body } = req;

    try {
      return res.status(StatusCodes.CREATED).json(
        await userRepository.save(<IUser>{
          ...body,
          password: await bcrypt.hash(body.password, 10),
        })
      );
    } catch (err) {
      next(err);
    }
  };

  delete: RequestHandler = async (_, res, next) => {
    try {
      return res.status(StatusCodes.OK).json();
    } catch (err) {
      next(err);
    }
  };

  update: RequestHandler = async (req, res, next) => {
    const { id } = req.params;

    try {
      res
        .status(StatusCodes.OK)
        .json(await userRepository.updateOne(id, req.body));
    } catch (err) {
      next(err);
    }
  };
}

export default new UserController();
