import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import { ILocations } from '../models/locations';
import locationsRepository from '../repositories/locations';

class LocationsController {
  index: RequestHandler = async (_, res, next) => {
    try {
      return res
        .status(StatusCodes.OK)
        .json(await locationsRepository.findAll());
    } catch (err) {
      next(err);
    }
  };

  showLocations: RequestHandler = async (_, res, next) => {
    const { userId } = res.locals.session;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await locationsRepository.findAllLocationsByUser(userId));
    } catch (err) {
      return next(err);
    }
  };

  saveLocation: RequestHandler = async (req, res, next) => {
    const { body } = req;
    const { userId } = res.locals.session;

    try {
      return res.status(StatusCodes.CREATED).json(
        await locationsRepository.saveLocation(<ILocations>{
          userId,
          ...body,
        })
      );
    } catch (err) {
      next(err);
    }
  };

  deleteLocation: RequestHandler = async (req, res, next) => {
    const { id } = req.params;

    try {
      return res
        .status(StatusCodes.OK)
        .json(await locationsRepository.deleteLocation(id));
    } catch (err) {
      next(err);
    }
  };
}

export default new LocationsController();
