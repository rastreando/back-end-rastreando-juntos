import mongoose, { Document, Schema } from 'mongoose';

// 0 - Saudável
// 1 - Contaminado
// 2 - Suspeito
export interface IDiagnostic extends Document {
  userId: string;
  data_resultado: Date;
  tipo_resultado: '0' | '1' | '2';
}

const DiagnosticSchema = new Schema({
  userId: { type: String, required: [true, 'please add a user Id'] },
  data_resultado: { type: Date, required: [true, 'please add a date result'] },
  tipo_resultado: {
    type: String,
    required: [true, 'please add the result kind'],
    enum: ['0', '1', '2'],
  },
  created_at: {
    type: Date,
    default: Date.now(),
  },
  updated_at: {
    type: Date,
  },
});

DiagnosticSchema.index({ userId: 1, data_resultado: 1 }, { unique: true });

export default mongoose.model<IDiagnostic>('diagnostic', DiagnosticSchema);
