//import geoJson from 'mongoose-geojson-schema';
import mongoose, { Document, Schema } from 'mongoose';

export interface ILocations extends Document {
  userId: string;
  data_coleta: Date;
  localizacao: {
    type: string;
    coordinates: [number];
  };
  precisao: number;
}

const locationsSchema = new Schema({
  userId: { type: String, required: [true, 'please add a userId'] },
  data_coleta: { type: Date, required: [true, 'please add collect date'] },
  localizacao: {
    type: {
      type: String,
      enum: ['Point'],
      required: [true, 'please add Point field for location'],
    },
    coordinates: {
      type: [Number],
      index: '2dsphere',
    },
  },
  precisao: { type: Number, required: [true, 'please add the precision'] },
  created_at: {
    type: Date,
    default: Date.now(),
  },
  updated_at: {
    type: Date,
  },
});

locationsSchema.index(
  { userId: 1, data_coleta: 1 },
  { unique: [true, 'please do not repeat insertions'] }
);

export default mongoose.model<ILocations>('locations', locationsSchema);
