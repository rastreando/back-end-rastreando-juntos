import mongoose, { Document, Schema } from 'mongoose';

//Retirar o campo caso_suspeito parece a melhor opcao (20/01/2021)
export interface ISelfEvaluation extends Document {
  userId: string;
  data_preenchimento: Date;
  foi_contaminado: boolean;
  caso_suspeito: boolean; // caso suspeito eh aquele que esta esperando o resultado de um teste, ou esta em triagem? pensar na necessidade desta informacao
  recebeu_vacina: boolean;
  febre_calafrios: boolean;
  mal_estar_geral: boolean;
  tosse_seca: boolean;
  dor_de_garganta: boolean;
  congestao_nasal: boolean;
  dificuldade_para_respirar: boolean;
  diarreia: boolean;
  nausea_vomitos: boolean;
  dor_de_cabeca: boolean;
  irritabilidade_confusao: boolean;
  perda_de_olfato: boolean;
  perda_de_paladar: boolean;
  dor_muscular: boolean;
  dor_abdominal: boolean;
  dor_no_peito: boolean;
  dor_nas_costas: boolean;
  dor_nas_articulacoes: boolean;
  diminuicao_do_apetite: boolean;
}

const SelfEvaluationSchema = new Schema({
  userId: { type: String, required: [true, 'please add a user Id'] },
  data_preenchimento: {
    type: Date,
    required: [true, 'please add the fill date'],
  },
  foi_contaminado: {
    type: Boolean,
    required: [true, 'please add if the user already was contaminated'],
  },
  caso_suspeito: { type: Boolean },
  recebeu_vacina: { type: Boolean },
  febre_calafrios: {
    type: Boolean,
    required: [true, 'please add fever chills symptom'],
  },
  mal_estar_geral: {
    type: Boolean,
    required: [true, 'please add general discomfort symptom'],
  },
  tosse_seca: {
    type: Boolean,
    required: [true, 'please add dry cough symptom'],
  },
  dor_de_garganta: {
    type: Boolean,
    required: [true, 'please add sore throat symptom'],
  },
  congestao_nasal: {
    type: Boolean,
    required: [true, 'please add nasal congestion symptom'],
  },
  dificuldade_para_respirar: {
    type: Boolean,
    required: [true, 'please add difficulty breathing symptom'],
  },
  diarreia: { type: Boolean, required: [true, 'please add diarrhea symptom'] },
  nausea_vomitos: {
    type: Boolean,
    required: [true, 'please add nausea symptom'],
  },
  dor_de_cabeca: {
    type: Boolean,
    required: [true, 'please add headache symptom'],
  },
  irritabilidade_confusao: {
    type: Boolean,
    required: [true, 'please add confusion symptom'],
  },
  perda_de_olfato: {
    type: Boolean,
    required: [true, 'please add loss of smell symptom'],
  },
  perda_de_paladar: {
    type: Boolean,
    required: [true, 'please add loss of taste symptom'],
  },
  dor_muscular: {
    type: Boolean,
    required: [true, 'please add muscle pain symptom'],
  },
  dor_abdominal: {
    type: Boolean,
    required: [true, 'please add abdominal pain symptom'],
  },
  dor_no_peito: {
    type: Boolean,
    required: [true, 'please add chest pain symptom'],
  },
  dor_nas_costas: {
    type: Boolean,
    required: [true, 'please add back pain symptom'],
  },
  dor_nas_articulacoes: {
    type: Boolean,
    required: [true, 'please add joint pain symptom'],
  },
  diminuicao_do_apetite: {
    type: Boolean,
    required: [true, 'please add decreased appetite symptom'],
  },
  created_at: {
    type: Date,
    default: Date.now(),
  },
  updated_at: {
    type: Date,
  },
});

SelfEvaluationSchema.index(
  { userId: 1, data_preenchimento: 1 },
  { unique: true }
);

export default mongoose.model<ISelfEvaluation>(
  'selfEvaluationForm',
  SelfEvaluationSchema
);
