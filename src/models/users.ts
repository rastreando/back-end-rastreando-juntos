//Documentar escolha dessas bibliotecas
import mongoose, { Document, Schema } from 'mongoose';

//Documentar melhor a definicao de tipo a partir do typeScript
export interface IUser extends Document {
  nome: string;
  username: string;
  email: string;
  idade: number;
  genero: 'M' | 'F' | 'O';
  comorbidades: {
    diabetes: boolean;
    hipertensao: boolean;
    insuficiencia_cardiaca: boolean;
    insuficiencia_renal: boolean;
    insuficiencia_respiratoria: boolean;
    outras: boolean;
  };
  residentes: number;
  senha: string;
}

const UserSchema = new Schema({
  nome: { type: String, required: true },
  username: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  senha: { type: String, required: true },
  idade: {
    type: Number,
    required: true,
    min: 1,
    max: 120,
  },
  genero: {
    type: String,
    required: true,
  },
  comorbidades: {
    type: {
      diabetes: { type: Boolean, required: true },
      hipertensao: { type: Boolean, required: true },
      insuficiencia_cardiaca: { type: Boolean, required: true },
      insuficiencia_renal: { type: Boolean, required: true },
      insuficiencia_respiratoria: { type: Boolean, required: true },
      outras: { type: Boolean, required: true },
    },
  },
  residentes: {
    type: Number,
    required: true,
    min: 0,
    max: 140,
  },
  created_at: {
    type: Date,
    default: Date.now(),
  },
  updated_at: {
    type: Date,
    default: Date.now(),
  },
});

UserSchema.index(
  { username: 1, email: 1 },
  { unique: [true, 'please do not repeat insertions'] }
);

export default mongoose.model<IUser>('User', UserSchema);
