import { config } from 'dotenv-safe';
config();

export const PORT = Number(process.env.PORT) || 3131;

export const HTTP_PORT = Number(process.env.PORT) + 1 || 3132;

export const HOST = process.env.HOST || 'localhost';

export const JWT_SECRET = process.env.JWT_SECRET || 'changeme';

export const DATABASE_URL = process.env.DATABASE_URL || '';
