<h1 align="center">Welcome to Rastreando Juntos Backend 👋</h1>
<p align="center">
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D7.5.4-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D14.15.4-blue.svg" />
  <img src="https://img.shields.io/badge/yarn-%3E%3D1.22.10-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> backend for Rastreando Juntos App

### 🏠 [Homepage](https://gitlab.com/rastreando/back-end-rastreando-juntos.git)

## Prerequisites

- npm >= 7.5.4
- node >= 14.15.4
- yarn >= 1.22.10

## Install

```sh
yarn
```

## Usage

Populate .env with secrets

```sh
cp .env.example .env
vim .env
```

Then execute

```sh
yarn start
```

## Run tests

- functional tests
	```sh
	yarn test
	```

- performance tests
	```sh
	cd tests/performance
	./run_load_test.sh <script>.yml
	```



## Author

👤 **Panaceia Inteligente**

* Gitlab: [@rastreando](https://gitlab.com/rastreando)
